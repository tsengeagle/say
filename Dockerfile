# syntax=docker/dockerfile:1

ARG BASE_IMAGE_TAG=3.2

FROM ruby:${BASE_IMAGE_TAG}-alpine AS base

ENV LANG="C.UTF-8"

ENV BUNDLE_FROZEN=true \
    BUNDLE_WITHOUT=development

WORKDIR /app

RUN apk add --no-cache imagemagick

RUN addgroup -S apps && adduser -S apps -G apps

FROM base AS development

ENV BUNDLE_FROZEN= \
    BUNDLE_WITHOUT=

# Define some default variables
ENV HISTFILE="/config/.bash_history" \
    GIT_COMMITTER_NAME="Just some fake name to be able to git-clone" \
    GIT_COMMITTER_EMAIL="whatever@this-user-is-not-supposed-to-git-push.anyway" \
    DISABLE_SPRING="true" \
    EDITOR="vim"

RUN apk add vim tmux ripgrep bash build-base curl htop tmux

# Add dot files to the home directory skeleton (they persist IRB/Pry/Rails console history, configure Yarn, etc…)
COPY dotfiles/* /etc/skel/

EXPOSE 80

# Use wrappers that check and maintain Ruby & JS dependencies (if necessary) as entrypoint
COPY docker-bin/* /usr/local/bin/

ENTRYPOINT ["bundler-wrapper"]

CMD ["bundle", "exec", "falcon", "serve", "-b", "http://0.0.0.0"]


FROM base AS bundler

COPY --chown=apps:apps Gemfile Gemfile.lock ./

RUN apk add --no-cache --virtual .build-deps \
            gcc \
            musl-dev \
            make \
            git \
    && bundle install -j $(nproc) \
    && bundle clean --force \
    && apk del .build-deps \
    && rm -rf $GEM_HOME/cache/*

FROM base AS all-files

COPY . .

FROM base as web

COPY --from=bundler --link /usr/local/bundle/ /usr/local/bundle/

COPY --from=all-files /app/ .

ENTRYPOINT ["bundle", "exec"]

CMD ["falcon", "host"]
